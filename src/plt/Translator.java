package plt;

import javax.print.DocFlavor.STRING;

public class Translator {
	
	public final static String NIL = "nil";
	private String phrase;
	private String phrase2="";
	private String[] words;
	private int NumberOfWords;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase(){
		
		return phrase;
	}

	public String translate() {
		if(phrase.contains(" ")) {
			String words[]=phrase.split(" ");
			NumberOfWords= words.length;
			Translator translator;
			
			for(int i=0; i<NumberOfWords-1; i++) {
				translator = new Translator(words[i]);
				phrase2= phrase2+translator.translate() + " ";
			}
			translator = new Translator(words[NumberOfWords-1]);
			phrase2=phrase2+translator.translate();
			return phrase2;
		}else {
			
			if(phrase.contains("-")) {
				String words[]=phrase.split("-");
				NumberOfWords= words.length;
				Translator translator;
				
				for(int i=0; i<NumberOfWords-1; i++) {
					translator = new Translator(words[i]);
					phrase2= phrase2+translator.translate() + "-";
				}
				translator = new Translator(words[NumberOfWords-1]);
				phrase2=phrase2+translator.translate();
				return phrase2;
			}
			
		}
		
		
		char[] str = phrase.toCharArray();
		
		if(startWithVowel()) {
			if(phrase.endsWith("y")) {
				
				if(PunctuationsContaining()) {
					
					char punctuation =phrase.charAt(phrase.length()-1);
					phrase2=phrase.substring(0,phrase.length()-1);
					
					if(phraseIsUpperCase()) {
						return phrase + "NAY" + punctuation;
					}else {
						
						if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
							
							return GetTitleCase(str);
						}else {
						return phrase + "nay" + punctuation;
						}
					}
				}
				
				if(phraseIsUpperCase()) {
					return phrase + "NAY";
				}else {	
					if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
						
						return GetTitleCase(str);
					}else {
					return phrase + "nay" ;
					}
				}
				
			}else if(endWithVowel()) {
				

				if(PunctuationsContaining()) {
					
					char punctuation =phrase.charAt(phrase.length()-1);
					phrase2=phrase.substring(0,phrase.length()-1);
					
					if(phraseIsUpperCase()) {
						return phrase + "YAY" + punctuation;
					}else {	
						
						if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
							
							return GetTitleCase(str);
						}else {
						return phrase + "yay" + punctuation;
						}
					}
				}
				
				if(phraseIsUpperCase()) {
					return phrase + "YAY";
				}else {	
					if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
						
						return GetTitleCase(str);
					}else {
					return phrase + "yay" ;
					}
				}
				
			}else if(!endWithVowel()) {
				
				if(PunctuationsContaining()) {
					
					char punctuation =phrase.charAt(phrase.length()-1);
					phrase2=phrase.substring(0,phrase.length()-1);
					
					if(phraseIsUpperCase()) {
						return phrase + "AY" + punctuation;
					}else {	
						if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
							
							return GetTitleCase(str);
						}else {
						return phrase + "ay" + punctuation;
						}
					}
				}
				
				if(phraseIsUpperCase()) {
					return phrase + "AY";
				}else {	
					if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
						
						return GetTitleCase(str);
					}else {
					return phrase + "ay" ;
					}
				}
				
				
			}
		}else if(phrase.isEmpty()) {
			return NIL;
		}else if(!startWithVowel() && startWithSingleConsonant()) {
			

			
			if(PunctuationsContaining()) {
				
				char punctuation =phrase.charAt(phrase.length()-1);
				phrase2=phrase.substring(0,phrase.length()-1);
				
				if(phraseIsUpperCase()) {
					phrase2=phrase2.substring(1)+str[0]+"AY";
				}else {
					if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
						
						return GetTitleCase(str);
					}else {
					phrase2=phrase2.substring(1)+str[0]+"ay";
					}
				}
				
				
				return phrase2=phrase2+ punctuation;
			}
			
			if(phraseIsUpperCase()) {
				return phrase2=phrase.substring(1)+str[0]+"AY";
			}else {	
				
				if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
					
					return GetTitleCase(str);
				}else {
					if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
						
						return GetTitleCase(str);
					}else {
					return phrase2=phrase.substring(1)+str[0]+"ay";
				}
				}
				
			}
			
		}else if(!startWithVowel() && !startWithSingleConsonant()) {
			
			int cont	=1;
			boolean check=false;
		
			
			do {
				if(!startsWithMoreConsonants(cont)) {
					cont++;
				}else {
					check=true;
				}
			}while(check=false);
	
			phrase2=phrase.substring(cont);
			
			if(PunctuationsContaining()) {
				
				char punctuation =phrase.charAt(phrase.length()-1);
				phrase2=phrase.substring(0,phrase.length()-1);
				for(int j=0; j<cont; j++ ) {
					phrase2=phrase2+str[j];
				}
				if(phraseIsUpperCase()) {
					phrase2=phrase2+"aY";
				}else {	
					if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
						
						return GetTitleCase(str);
					}else {
					phrase2=phrase2+"ay";
					}
					}
				
				
				return phrase2=phrase2+ punctuation;
			}else {
				for(int j=0; j<cont; j++ ) {
					phrase2=phrase2+str[j];
				}
				
				if(phraseIsUpperCase()) {
					phrase2=phrase2+"aY";
				}else {	
					if(Character.isUpperCase(str[0]) && !Character.isUpperCase(str[1])) {
						
						return GetTitleCase(str);
					}else {
					phrase2=phrase2+"ay";
				}
					}
			}

			}
		
		
		return phrase2;
	}
	
	
	

	
	private String GetTitleCase(char[] st) {
		
		if(Character.isUpperCase(st[0]) && !Character.isUpperCase(st[1])) {
			
			
			
			phrase2= Character.toUpperCase(st[1])+phrase.substring(2)+ Character.toLowerCase(st[0]) +"ay";
		}
		return phrase2;
		
	}

	private boolean phraseIsUpperCase() {
		int	checkCase=0; //UPPER-CASE
		char[] str = phrase.toCharArray();
		
		for(int i=0; i<phrase.length(); i++) {
			if(!Character.isUpperCase(str[i])) {
				checkCase=1; //NOT UPPER-CASE
			}
		}
		if(checkCase==0) {
			return true;
		}
		return false;
	}

	private boolean PunctuationsContaining() {

		return phrase.endsWith(".") || phrase.endsWith(",") || phrase.endsWith(";") || phrase.endsWith(":") || phrase.endsWith("?") || phrase.endsWith("!") || 
				phrase.endsWith("'") || phrase.endsWith("(") || phrase.endsWith(")");
	}

	private boolean startsWithMoreConsonants(int i) {
		return phrase.startsWith("a",i) || phrase.startsWith("e",i) || phrase.startsWith("i",i) || phrase.startsWith("o",i) || phrase.startsWith("u",i);
	}
	
	private boolean startWithSingleConsonant() {
		return phrase.startsWith("a",1) || phrase.startsWith("e",1) || phrase.startsWith("i",1) || phrase.startsWith("o",1) || phrase.startsWith("u",1);
	}

	private boolean startWithVowel() {
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u") ||
				 phrase.startsWith("A") ||  phrase.startsWith("E") ||  phrase.startsWith("I") ||  phrase.startsWith("O") ||  phrase.startsWith("U");
	}
	
	private boolean endWithVowel() {
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u") ||
				phrase.endsWith("A") || phrase.endsWith("E") || phrase.endsWith("I") || phrase.endsWith("O") || phrase.endsWith("U");
	}
}
